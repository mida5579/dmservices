$(document).ready(function () {

    $("#sidebarResponsiveClose").on("click",(e)=>{
        e.stopPropagation();

        $(".sidebar").toggleClass('sidebar--active')
    })
    
    $("#sidebarResponsive").on("click",()=>{
        
        $(".sidebar").removeClass('sidebar--active')
    })

    $(window).on("click",()=>{
        
        $(".sidebar").removeClass('sidebar--active')
    })
    
})